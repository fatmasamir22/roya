import axios from "axios";
import JwtService from "../../plugins/JwtService";

export default {
  state: () => ({
  }),
  mutations: {
    SET_AUTH(user) {
      console.log("user ==== ",user)
      JwtService.saveUser(user);
    },
    SET_LOGOUT() {
      JwtService.destroyToken();
    },
  },
  actions: {
    async Login(context, data) {
      return await new Promise((resolve, reject) => {
        axios
          .post(
            "https://erp-development.rasiderp.com/erp_link/login",
            data,
            {
              headers: {
                Accept: "application/json",
                "Content-type": "application/json",
              },
            }
          )
          .then((res) => {
      JwtService.saveUser(res.data.data);
            resolve();
          })
          .catch((error) => {
            reject(error.response.data.message);
          });
      });
    },

    async Logout(context) {
      let token = JwtService.getToken();
      context.commit("SET_LOGOUT");
      return await new Promise((resolve, reject) => {
        axios
          .post(
            "https://erp-development.rasiderp.com/user_prefix/logout",
            token,
            {
              headers: {
                Accept: "application/json",
                "Content-type": "application/json",
                ["Authorization"]: `Bearer ${token}`,
              },
            }
          )
          .then(() => {
            JwtService.destroyToken();
            resolve();
          })
          .catch((error) => {
            reject(error.response.data.message);
          });
      });
    },

    resetPagination({ commit }) {
      commit("RESET_PAGINATION");
    },
  },
  getters: {
  },
};
