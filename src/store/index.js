import Vue from "vue";
import Vuex from "vuex";
import auth from "./modules/auth";
import Lang from "./modules/lang";
Vue.use(Vuex);

export default new Vuex.Store({
  nameSpaced: false,
  modules: {
    Lang,
    auth
  },
  // getters: getters
});
