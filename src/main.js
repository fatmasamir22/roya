import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify' 
import i18n from "@/lang"; 
import "./bootstrap";
import Vuelidate from 'vuelidate'
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';
import ListTop from "@/components/SlideBar/ListTop.vue"
import "@/assets/scss/main.scss"
import ListGroup from "@/components/SlideBar/ListGroup.vue"
import ListItems from "@/components/SlideBar/ListItems.vue"



Vue.component('ListGroup', ListGroup);
Vue.component('ListItems', ListItems);
Vue.component('ListTop', ListTop);
// import http from './plugins/http.js'

// Vue.use(http)


Vue.use(Vuelidate)
Vue.use(VueToast,{
  position: 'top'
});
 
Vue.config.productionTip = false



new Vue({
  router,
  store,
  i18n,
  vuetify,
  render: h => h(App)
}).$mount('#app')
