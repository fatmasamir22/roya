import Vue from "vue";
import VueI18n from "vue-i18n";

import mainEn from "./mainEn.js"; 
import main from "./main.js"; 

Vue.use(VueI18n);

export const messages = {
  en: {
    ...mainEn
  },
  ar: {
    ...main
  }
};

const i18n = new VueI18n({
  legacy: false,
  locale:  localStorage.getItem("lang") || "ar",
  globalInjection: true,
  messages
});

export default i18n;
