export default {
 ar:{
  headers: {
    home:"الصفحه الرئسيه",
    Lang:'English'
  },
  breadcrumbs: {
    home:"الصفحه الرئسيه",
  },
  validation: {},
  buttons: {
  },
  slidebar: {
    dashboard:"لوحة التحكم",
    customers:"العملاء"

  },
  label: {
    Title:"احصائيات",
     search:"ابحث عن .."
     
  },
  placeholder: {

  },
  table: {
  },
  chips: {
  },
 },
 en:{
  headers: {
    home:"Home Page",
    Lang:'عربى'
  },
  breadcrumbs: {
    home:"Home "
  },
  validation: {},
  buttons: {
  },
  slidebar: {
    dashboard:"dashboard",
    customers:"customers"
  },
  label: {
    Title:"statistics",search:" search for .."
     
  },
  placeholder: {

  },
  table: {
  },
  chips: {
  },
 }
};
