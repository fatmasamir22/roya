import i18n from "@/lang";
import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'


Vue.use(VueRouter)

const constantRouterMap = [

  {
    path: '/',
    redirect: to => {
      // the function receives the target route as the argument
      // we return a redirect path/location here.
      return { path: '/home', query: { q: to.params.searchText } }
    },
  },


  {
    path: '/home',
    name: 'home',
    component: HomeView ,
    meta: {
      requiresAuth: true ,
      title: i18n.t("Home.breadcrumbs.home") ,
      bread_crumb: [
        {
          text: i18n.t("Home.breadcrumbs.home"),
          display:false
        }
      ]
    }
  },

]


const router = new VueRouter({
  mode: "hash",
  routes: constantRouterMap,
})



// import JwtService from "../plugins/JwtService";

// router.beforeEach((to, from, next) => {
//   if( to.matched.some( record => record.meta.requiresAuth )){ 
//       if(JwtService.getToken() == null){ 
//           next({
//             path:'/login'
//           }) 
//       }else{ 
//         next();
//       }
//   }else{  
//     next();
//   }
router.beforeEach((to, from, next) => {
  document.title = to.meta.title
  next()
})
// });
// router.beforeEach((to, from, next) => {
//   if( to.matched.some( record => record.meta.requireNotAuth )){ 
//       if(JwtService.getToken() != null){ 
//           next({
//               path:'/home'
//           }) 
//       }else{
//           next();
//       }
//   }else{ 
//       next();
//   }
// });


export default router
