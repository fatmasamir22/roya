import * as axios from "axios";

import JwtService from "./JwtService";
const httpClient = axios.create();
httpClient.defaults.baseURL = "https://erp-development.rasiderp.com/erp_link/";
httpClient.defaults.timeout = "";

httpClient.interceptors.response.use(
  function (response) {
    return response.data;
  },
  function (error) {
    if (error.response) {
      if (error.response.status === 401) {
        //dispatch logout action
      } else {
        return Promise.reject(error.response.data);
      }
    } else if (error.message) {
      //error.message
      return Promise.reject(error.message);
    }
  }
);

export function setRequestHeader() {
  const token = localStorage.getItem("token");
  if (!token) {
    httpClient.defaults.headers.post["Content-Type"] ="application/json";
    httpClient.defaults.headers.post["Accept"] ="application/json";
  } else {
    httpClient.defaults.headers.post["Content-Type"] ="application/json";
    httpClient.defaults.headers.post["Accept"] ="application/json";
    httpClient.defaults.headers.post["Authorization"] ="Bearer " + token;
  }
}

export function HtttpGetDefult(url) {
  setRequestHeader();
  return new Promise((resolve, reject) => {
    httpClient
      .get(url,{
        headers: {
          "Accept": "application/json",
          "Content-type": "application/json",
          "Authorization" : "Bearer " + JwtService.getToken()
      }
      })
      .then((res) => {
        if(res == undefined){
          JwtService.destroyToken();
        }
        resolve(res);
      })
      .catch((error) => {
        reject(error);
        
      });
  });
}
export function HtttpGetFormEmployee(url) {
  setRequestHeader();
  return new Promise((resolve, reject) => {
    httpClient
      .get(url,{
        headers: {
          "Accept": "application/json",
          "Content-type": "application/json",
      }
      })
      .then((res) => {
        if(res == undefined){
          JwtService.destroyToken();
        }
        resolve(res);
      })
      .catch((error) => {
        reject(error);
      });
  });
}
export function HtttpGetExport(url) {
  setRequestHeader();
  return new Promise((resolve, reject) => {
    httpClient
      .get(url,{
        headers: {
          "Accept": "application/json",
          "Content-type": "application/json",
          "Authorization" : "Bearer " + JwtService.getToken()
      }
      })
      .then((res) => {
        if(res == undefined){
          JwtService.destroyToken();
        }
        resolve(res);
      })
      .catch((error) => {
        reject(error);
        
      });
  });
}

export function HtttpPostDefult(url, data) {
  setRequestHeader();
  return new Promise((resolve, reject) => {
    httpClient
      .post( url, data,{
        headers: {
          "Accept": "application/json",
          "Content-type": "application/json",
          "Authorization" : "Bearer " + JwtService.getToken()
      }
      })
      .then((res) => {
        resolve(res);
      })
      .catch((error) => {
        console.log("error ==== ",error)
        if(error.message == "Unauthenticated."){
          JwtService.destroyToken();
         this.$router.push("/login");
         }else{
        reject(error);
         }
      });
  });
}

export function HtttpPostFormEmployee(url, data) {
  setRequestHeader();
  return new Promise((resolve, reject) => {
    httpClient
      .post( url, data,{
        headers: {
          "Accept": "application/json",
          "Content-type": "multipart/form-data",
      }
      })
      .then((res) => {
        resolve(res);
      })
      .catch((error) => {
        console.log("error ==== ",error)
        if(error.message == "Unauthenticated."){
          JwtService.destroyToken();
         this.$router.push("/login");
         }else{
        reject(error);
         }
      });
  });
}
export function HtttpDeleteDefult(url) {
  setRequestHeader();
  return new Promise((resolve, reject) => {
    httpClient
      .delete( url,{
        headers: {
          "Accept": "application/json",
          "Content-type": "application/json",
          "Authorization" : "Bearer " + JwtService.getToken()
      }
      })
      .then((res) => {
        if(res == undefined){
          JwtService.destroyToken();
        }
        resolve(res);
      })
      .catch((error) => {
        reject(error);
        
      });
  });
}

export function HtttpPutDefult(url, data) {
  setRequestHeader();
  return new Promise((resolve, reject) => {
    httpClient
      .put(url, data,{
        headers: {
          "Accept": "application/json",
          "Content-type": "application/json",
          "Authorization" : "Bearer " + JwtService.getToken()
      }
      })
      .then((res) => {
        if(res == undefined){
          JwtService.destroyToken();
        }
        resolve(res);
      })
      .catch((error) => {
        reject(error);
        
      });
  });
}export function HtttpPutFormEmployeet(url, data) {
  setRequestHeader();
  return new Promise((resolve, reject) => {
    httpClient
      .put(url, data,{
        headers: {
          "Accept": "application/json",
          "Content-type": "application/json",
      }
      })
      .then((res) => {
        if(res == undefined){
          JwtService.destroyToken();
        }
        resolve(res);
      })
      .catch((error) => {
        reject(error);
        
      });
  });
}
export function HtttpPatchDefult(url, data) {
  setRequestHeader();
  return new Promise((resolve, reject) => {
    httpClient
      .patch(url, data,{
        headers: {
          "Accept": "application/json",
          "Content-type": "application/json",
          "Authorization" : "Bearer " + JwtService.getToken()
      }
      })
      .then((res) => {
        if(res == undefined){
          JwtService.destroyToken();
        }
        resolve(res);
      })
      .catch((error) => {
        reject(error);
        
      });
  });
}

export default httpClient;
