
export const getToken = () => {
  return localStorage.getItem("token");
};
/**
 * @description save token into localStorage
 * @param token: string
 */
export const saveToken = (token) => {
  localStorage.setItem('token', token);
};

export const saveUser = (user) => {
  localStorage.setItem('token', user.token);
    localStorage.setItem('userAuth',  JSON.stringify(user));
};

// export const saveUpdateUser = (user): void => {
//   localStorage.setItem('name', user.name);
//   localStorage.setItem('email', user.email);
//   localStorage.setItem('key', user.id);
//   localStorage.setItem('role', user.role);
//   localStorage.setItem('lang', 'ar');
// };
export const destroyToken = () => {
  localStorage.removeItem('token');
  localStorage.removeItem('userAuth');
};

export default {  getToken, saveToken, destroyToken ,saveUser };
